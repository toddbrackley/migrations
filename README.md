## Installation

Ensure that you have installed your nuget packages - this is done automatically if you start with build.bat. After that msbuild also always checks

## Credentials
 
If you are entering your git credentials each time, don't forget you have ssh-add (or use the scripts described here: https://help.github.com/articles/working-with-ssh-key-passphrases#platform-windows)
