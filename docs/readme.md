[TOC]

# Overview

## Features

There are four features to be developed. We have provisional release approach.

![Features to be developed](https://bytebucket.org/toddbrackley/migrations/raw/e71f4d6443c2d7e3c9d9f85a2bdb61edff3b6988/docs/assets/features.jpg?token=c2796f7378f5030e5fcee20c11b9bf7b27cca399 "I'm broken, click on me to see file in assets")

## Brand and tags to releases

We are going to develop on `dev` and then release off `master`. `dev` is thus the current release. We are also going to maintian a current-1 branch which happens to be called `3x` after the fact it was a 3.0 release! We will tag every release.

Note: Three releases (3.0, 4.0, 4.1) are all off `master` and the previous release is not released from their but a release branch `3x`.

![Branching approach for features](https://bytebucket.org/toddbrackley/migrations/raw/e71f4d6443c2d7e3c9d9f85a2bdb61edff3b6988/docs/assets/branches-and-tags.jpg?token=f6bbfc975a16a52822633dc1325d237cdc126660 "I'm broken, click on me to see file in assets")

## Release Schedule for clients

We have a whole range of clients who all want releases in different orders. We know the order they are likely to want it and which features are in each release.

![Release to cleints](https://bytebucket.org/toddbrackley/migrations/raw/e71f4d6443c2d7e3c9d9f85a2bdb61edff3b6988/docs/assets/release-schedule.jpg?token=5d15b8dfeadf1379d16a1bf44c0dab7c4fd03da5 "I'm broken, click on me to see file in assets")

# Development

## Create a dev and master branch

In this case, we had started on master, we are now only going to release off master. 

	$ git branch
	* master

	/migrations (master)
	$ git checkout -b dev master
	Switched to a new branch 'dev'

	/migrations (dev)
	$ git branch
	* dev
	  master

## Add feature 1: Create User

So we've actually now written and tested some code

	$ git commit -am "feat(): create user"
	[dev 0873cdd] feat(): create user
	 1 file changed, 2 insertions(+), 2 deletions(-)

### Release 1: 3.0

	$ git checkout master
	$ git merge dev
	$ git tag -a "v3.0" -m "Version 3.0"

	/migrations (master)
	$ git push origin --all
	Counting objects: 13, done.
	Delta compression using up to 2 threads.
	Compressing objects: 100% (6/6), done.
	Writing objects: 100% (7/7), 603 bytes | 0 bytes/s, done.
	Total 7 (delta 3), reused 0 (delta 0)
	To git@bitbucket.org:m/migrations.git
	   54111b8..0873cdd  dev -> dev
	   54111b8..0873cdd  master -> master

	/migrations (master)
	$ git push origin --tags
	Counting objects: 1, done.
	Writing objects: 100% (1/1), 159 bytes | 0 bytes/s, done.
	Total 1 (delta 0), reused 0 (delta 0)
	To git@bitbucket.org:m/migrations.git
	 * [new tag]         v3.0 -> v3.0

## Add feature 2: Add Password - feature request on the released 3.0 branch

	/migrations (master)
	$ git checkout dev
	Switched to branch 'dev'

## Create our feature branch: 3x

	/migrations (dev)
	$ git checkout -b 3x dev
	Switched to a new branch '3x'

	/migrations (3x)
	$ git branch
	* 3x
	  dev
	  master

add our code for password (at this stage the release date hasn't been scheduled)

	/migrations (3x)
	$ git add src/Infrastructure/Db/Migrations/Migr_002_AddPassword_Column.cs

	$ git commit -am "feat(): add password"
	[3x 4d152f5] feat(): add password
	 1 file changed, 20 insertions(+)
	 create mode 100644 src/Infrastructure/Db/Migrations/Migr_002_AddPassword_Column
	.cs

	$ git add src/Infrastructure/Db/Migrations/Migr_002_RemoveMiddle_Column.cs

## Add feature 3: Remove Middlename

We code our solution back on dev

	$ git checkout dev

	/migrations (dev)
	$ git branch
	  3x
	* dev
	  master

	/migrations (dev)
	$ git commit -am "feat(): remove middlename"
	[dev 334cba0] feat(): remove middlename
	 2 files changed, 22 insertions(+)
	 create mode 100644 src/Infrastructure/Db/Migrations/Migr_002_RemoveMiddle_Colum
	n.cs

	/migrations (dev)
	$ git status
	On branch dev
	Changes not staged for commit:
	  (use "git add <file>..." to update what will be committed)
	  (use "git checkout -- <file>..." to discard changes in working directory)

	        modified:   src/Infrastructure/Infrastructure.csproj

	Untracked files:
	  (use "git add <file>..." to include in what will be committed)

	        src/Infrastructure/Db/Migrations/Migr_002_RemoveMiddle_Column.cs

	no changes added to commit (use "git add" and/or "git commit -a")

	/migrations (dev)
	$ git add src/Infrastructure/Db/Migrations/Migr_002_RemoveMiddle_Column.cs

	/migrations (dev)
	$ git commit -am "feat(): remove middlename"
	[dev 334cba0] feat(): remove middlename
	 2 files changed, 22 insertions(+)
	 create mode 100644 src/Infrastructure/Db/Migrations/Migr_002_RemoveMiddle_Colum
	n.cs

## make these changes available back to the team

	/migrations (dev)
	$ git push origin dev
	Counting objects: 19, done.
	Delta compression using up to 2 threads.
	Compressing objects: 100% (7/7), done.
	Writing objects: 100% (8/8), 1.00 KiB | 0 bytes/s, done.
	Total 8 (delta 2), reused 0 (delta 0)
	To git@bitbucket.org:m/migrations.git
	   0873cdd..334cba0  dev -> dev

### Release 2: 4.0

	/migrations (dev)
	$ git checkout master
	Switched to branch 'master'
	Your branch is up-to-date with 'origin/master'.

	/migrations (master)
	$ git merge dev
	Updating 0873cdd..334cba0
	Fast-forward
	 .../Db/Migrations/Migr_002_RemoveMiddle_Column.cs   | 21 +++++++++++++++++++++
	 src/Infrastructure/Infrastructure.csproj            |  1 +
	 2 files changed, 22 insertions(+)
	 create mode 100644 src/Infrastructure/Db/Migrations/Migr_002_RemoveMiddle_Colum
	n.cs

	/migrations (master)
	$ git tag -a "v4.0" -m "Version 4.0"

### Release 3: 3.1

We now have a release schedule and it is deemed ready to be released!

	/migrations (master)
	$ git checkout 3x
	Switched to branch '3x'

	/migrations (3x)
	$ git branch
	* 3x
	  dev
	  master

	/migrations (3x)
	$ git tag -a "v3.1" -m "Version 3.1"

	/migrations (dev)
	$ git push origin --all
	Total 0 (delta 0), reused 0 (delta 0)
	To git@bitbucket.org:m/migrations.git
	   0873cdd..334cba0  master -> master

	/migrations (dev)
	$ git push origin --tags
	Counting objects: 2, done.
	Delta compression using up to 2 threads.
	Compressing objects: 100% (2/2), done.
	Writing objects: 100% (2/2), 287 bytes | 0 bytes/s, done.
	Total 2 (delta 0), reused 0 (delta 0)
	To git@bitbucket.org:m/migrations.git
	 * [new tag]         v3.1 -> v3.1
	 * [new tag]         v4.0 -> v4.0

## Add feature 4: Add nickname (and also password)

## Merge feature from 3x

	/migrations (3x)
	$ git checkout dev
	Switched to branch 'dev'

	/migrations (dev)
	$ git branch
	  3x
	* dev
	  master

	/migrations (dev)
	$ git merge 3x
	Merge made by the 'recursive' strategy.
	 .../Db/Migrations/Migr_002_AddPassword_Column.cs     | 20 ++++++++++++++++++++
	 1 file changed, 20 insertions(+)
	 create mode 100644 src/Infrastructure/Db/Migrations/Migr_002_AddPassword_Column
	.cs

Now also add the file to the solution

	/migrations (dev)
	$ git status
	On branch dev
	Changes not staged for commit:
	  (use "git add <file>..." to update what will be committed)
	  (use "git checkout -- <file>..." to discard changes in working directory)

	        modified:   src/Infrastructure/Infrastructure.csproj

	no changes added to commit (use "git add" and/or "git commit -a")

Commit the merge before you do new work

	/migrations (dev)
	$ git commit -am "feat(): merge from 3x"
	[dev 3f470eb] feat(): merge from 3x
	 1 file changed, 1 insertion(+)

Now, add your new feature

	/migrations (dev)
	$ git status
	On branch dev
	Changes not staged for commit:
	  (use "git add <file>..." to update what will be committed)
	  (use "git checkout -- <file>..." to discard changes in working directory)

	        modified:   src/Infrastructure/Infrastructure.csproj

	Untracked files:
	  (use "git add <file>..." to include in what will be committed)

	        src/Infrastructure/Db/Migrations/Migr_003_AddNickname_Column.cs

	no changes added to commit (use "git add" and/or "git commit -a")

	/migrations (dev)
	$ git add src/Infrastructure/Db/Migrations/Migr_003_AddNickname_Column.cs

	/migrations (dev)
	$ git commit -am "feat(): add nickname
	> "
	[dev 50c773f] feat(): add nickname
	 2 files changed, 21 insertions(+)
	 create mode 100644 src/Infrastructure/Db/Migrations/Migr_003_AddNickname_Column
	.cs

### Release 4: 4.1

	/migrations (dev)
	$ git branch master
	fatal: A branch named 'master' already exists.

	/migrations (dev)
	$ git checkout master
	Switched to branch 'master'
	Your branch is up-to-date with 'origin/master'.

	/migrations (master)
	$ git merge dev
	Updating 334cba0..50c773f
	Fast-forward
	 .../Db/Migrations/Migr_002_AddPassword_Column.cs     | 20 ++++++++++++++++++++
	 .../Db/Migrations/Migr_003_AddNickname_Column.cs     | 20 ++++++++++++++++++++
	 src/Infrastructure/Infrastructure.csproj             |  2 ++
	 3 files changed, 42 insertions(+)
	 create mode 100644 src/Infrastructure/Db/Migrations/Migr_002_AddPassword_Column
	.cs
	 create mode 100644 src/Infrastructure/Db/Migrations/Migr_003_AddNickname_Column
	.cs

	/migrations (master)
	$ git tag -a "v4.1" -m "Version 4.1"

	/migrations (master)
	$ git checkout dev
	Switched to branch 'dev'

	/migrations (dev)
	$ git branch
	  3x
	* dev
	  master

	/migrations (dev)
	$ git push origin --all
	Counting objects: 26, done.
	Delta compression using up to 2 threads.
	Compressing objects: 100% (7/7), done.
	Writing objects: 100% (8/8), 990 bytes | 0 bytes/s, done.
	Total 8 (delta 3), reused 0 (delta 0)
	To git@bitbucket.org:m/migrations.git
	   3f470eb..50c773f  dev -> dev
	   334cba0..50c773f  master -> master

	/migrations (dev)
	$ git push origin --tags
	Counting objects: 1, done.
	Writing objects: 100% (1/1), 161 bytes | 0 bytes/s, done.
	Total 1 (delta 0), reused 0 (delta 0)
	To git@bitbucket.org:m/migrations.git
	 * [new tag]         v4.1 -> v4.1

Done!
