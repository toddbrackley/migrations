﻿using FluentMigrator;

namespace Infrastructure.Db.Migrations
{
    [Migration(201405221600)]
    public class Migr_003_AddNickname_Column : Migration
    {

        public override void Up()
        {
            Alter.Table("Users")
                .AddColumn("Nickname").AsString(32).NotNullable();
        }

        public override void Down()
        {
            Delete.Column("Nickname").FromTable("Users");
        }
    }
}