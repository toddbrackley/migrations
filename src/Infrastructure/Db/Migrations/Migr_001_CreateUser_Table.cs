﻿using FluentMigrator;

namespace Infrastructure.Db.Migrations
{
    [Migration(201405201500)]
    public class Migr_001_CreateUser_Table : Migration
    {

        public override void Up()
        {
            Create.Table("Users")
                .WithColumn("UserId").AsInt32().Identity().PrimaryKey()
                .WithColumn("First").AsString(32).NotNullable()
                .WithColumn("Middle").AsString(32).NotNullable()
                .WithColumn("Last").AsString(32).NotNullable();
        }

        public override void Down()
        {
            Delete.Table("Users");
        }
    }
}