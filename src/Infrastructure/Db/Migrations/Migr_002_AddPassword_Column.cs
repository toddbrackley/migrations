﻿using FluentMigrator;

namespace Infrastructure.Db.Migrations
{
    [Migration(201405211500)]
    public class Migr_002_AddPassword_Column : Migration
    {

        public override void Up()
        {
            Alter.Table("Users")
                .AddColumn("Password").AsString(32).NotNullable();
        }

        public override void Down()
        {
            Delete.Column("Password").FromTable("Users");
        }
    }
}