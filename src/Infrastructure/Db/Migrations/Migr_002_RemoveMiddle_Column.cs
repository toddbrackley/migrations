﻿using FluentMigrator;

namespace Infrastructure.Db.Migrations
{
    [Migration(201405221500)]
    public class Migr_002_RemoveMiddle_Column : Migration
    {

        public override void Up()
        {
            Delete.Column("Middle").FromTable("Users");

        }

        public override void Down()
        {
            Alter.Table("Users")
                .AddColumn("Middle").AsString(32).NotNullable();
        }
    }
}